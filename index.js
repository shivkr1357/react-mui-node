const express = require("express");
const app = express();

const dotenv = require("dotenv");
const mongoose = require("mongoose");

const PostRoutes = require("./Routes/posts");
const CategoryRoutes = require("./Routes/categories");
const UserRoutes = require("./Routes/user");
const AuthRoutes = require("./Routes/auth");

const bodyParser = require("body-parser");
const multer = require("multer");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/images/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname);
  },
});

var upload = multer({ storage: storage });

const cors = require("cors");

dotenv.config();

app.use(express.json());
app.use("uploads/images/", express.static("uploads/images/"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

if (process.env.NODE_ENV == "production") {
  const path = require("path");
  app.get("/", (req, res) => {
    app.use(express.static(path.join(__dirname, "./frontend/build")));
    res.sendFile(path.join(__dirname, "./frontend/build/index.html"));
  });
}

mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(console.log("connected to Mongo database "))
  .catch((err) => {
    console.log(err);
  });

app.use("/api/posts", upload.single("photo"), PostRoutes);
app.use("/api/categories", CategoryRoutes);
app.use("/api/auth", upload.none(), AuthRoutes);

app.use("/api/users", upload.single("profile_pic"), UserRoutes);

app.listen(process.env.PORT || 3001, () => {
  console.log(`Server Listening on the port ${process.env.PORT}`);
});
