const User = require("../Models/User");

exports.updateUser = async (req, res) => {
  try {
    let { first_name, last_name, email, username, password } = req.body;

    let profile_pic = req.file ? req.file.filename : null;

    let data = await User.findByIdAndUpdate({
      first_name: first_name,
      last_name: last_name,
      username: username,
      email: email,
      password: password,
      profile_pic: profile_pic,
    });
    const user = await data.save();
    res.status(200).json(user);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};
