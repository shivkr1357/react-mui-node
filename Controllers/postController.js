const Post = require("../Models/Posts");
const minifaker = require("minifaker");
require("minifaker/locales/en");

// Create Post

exports.create = async (req, res) => {
  console.log(req.user);
  let { title, desc } = req.body;
  let photo = req.file ? req.file.filename : null;

  try {
    const newPost = new Post({
      title: title,
      desc: desc,
      photo: photo,
      user: req.user.id,
    });

    const post = await newPost.save();

    res.status(200).json(post);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

//Fetch All

exports.getPosts = async (req, res) => {
  try {
    const posts = await Post.find({});

    res.status(200).json(posts);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

//Update Post

exports.updatePost = async (req, res) => {
  try {
    const newPost = await Post.findByIdAndUpdate(req.params.id, {
      title: req.body.title,
      desc: req.body.desc,
      photo: req.body.photo,
      user: req.user.id,
    });

    const post = await newPost.save();

    res.status(200).json(post);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

// Delete Post

exports.deletePost = async (req, res) => {
  try {
    const newPost = await Post.findByIdAndDelete(req.params.id);

    res.status(200).json({ message: "Post Deleted successfully" });
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

//Find one Post

exports.findOne = async (req, res) => {
  console.log(req.body);
  try {
    const newPost = await Post.findById(req.params.id);

    res.status(200).json(newPost);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

// find User Posts :

exports.getUserPosts = async (req, res) => {
  try {
    const posts = await Post.find({});

    res.status(200).json(posts);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

//  Infinite Scroll Posts

exports.getInfinitePost = async (req, res) => {
  const skip = req.query.skip ? Number(req.query.skip) : 0;

  const DEFAULT_LIMIT = 10;

  try {
    const posts = await Post.find({}).skip(skip).limit(DEFAULT_LIMIT);

    res.status(201).json({ data: posts, success: true });
  } catch (error) {
    res.status(400).json({ message: error });
  }
};

exports.fakeDataCreation = async (req, res) => {
  const { total } = req.body;
  const posts = [];
  try {
    const compilePost = async (_) => {
      for (index = 0; index < total; index++) {
        // const randomAvatarNum = Math.floor(Math.random() * 70) + 1;
        const randomImageNum = Math.floor(Math.random() * 70) + 1;

        const post = new Post({
          title: minifaker.username(),
          desc: `${minifaker.word()} ${minifaker.word()} ${minifaker.cityName()}`,
          photo: `https://i.pravatar.cc/600?img=${randomImageNum}`,
          // categories: `${minifaker.cityName()}`,
        });

        posts.push(post);
      }
    };

    await compilePost();
    await Post.insertMany(posts);

    res.status(201).json({
      success: true,
    });
  } catch (error) {
    res.status(400).json({ error: error });
  }
};
