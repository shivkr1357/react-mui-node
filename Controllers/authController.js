const User = require("../Models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const asyncHandler = require("express-async-handler");

exports.register = asyncHandler(async (req, res) => {
  //destructure the data from body
  let { email, username, password } = req.body;

  //check if the fields are empty ( Validation)
  if (!password || !email || !username) {
    res.status(400).json({ message: "Please fill all the fields" });
  }
  // Check if user already exists
  const userExists = await User.findOne({ email });

  if (userExists) {
    res.status(400).json({ message: "User Already exists" });
  }

  //Gen salt and hash password

  const salt = await bcrypt.genSalt(10);

  const hashedPassword = await bcrypt.hash(password, salt);

  let user = await User.create({
    username: username,
    email: email,
    password: hashedPassword,
  });
  if (user) {
    res.status(200).json({
      _id: user.id,
      username: user.username,
      email: user.email,
      token: genToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error("User not created");
  }
});

// Generate JWT token

const genToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: "1h",
  });
};

exports.login = asyncHandler(async (req, res) => {
  // console.log(req.body);

  const { email, password } = req.body;

  if (!email || !password) {
    res.status(400).json({ message: "Please Enter email and password" });
  }

  const user = await User.findOne({ email });

  if (user && (await bcrypt.compare(password, user.password))) {
    res.json({
      _id: user.id,
      username: user.username,
      email: user.email,
      token: genToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error("Invalid Credentials");
  }
});

exports.getUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id).select("-password");

  res.status(200).json(user);
});
