const Category = require("../Models/Category");

//Fetch all
exports.fetchAll = async (req, res) => {
  try {
    const categories = await Category.find();

    res.status(200).json(categories);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

// Create category

exports.createCategories = async (req, res) => {
  try {
    const categories = new Category({
      name: req.body.name,
    });

    const newCategory = await categories.save();

    res.status(200).json(newCategory);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};
