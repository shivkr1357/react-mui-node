import { Box, createTheme, Stack, ThemeProvider } from "@mui/material";
import LeftBar from "../components/LeftBar";
import Feed from "../components/Feed";
import Rightbar from "../components/Rightbar";
import Navbar from "../components/Navbar";
import { useState } from "react";
import { useEffect } from "react";

import Footer from "../components/Footer";
import { fetchPosts } from "../apis/posts";
import { setPosts } from "../redux/actions/postActions";
import { useDispatch } from "react-redux";

const Home = () => {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });

  const dispatch = useDispatch();

  useEffect(() => {
    fetchPosts()
      .then((res) => {
        dispatch(setPosts(res.data));
        // console.log(res.data);
      })
      .catch((err) => {
        console.log("error : ", err);
      });
  }, [dispatch]);

  return (
    <ThemeProvider theme={darkTheme}>
      <Box bgcolor={"background.default"} color={"text.primary"}>
        <Navbar setMode={setMode} mode={mode} />
        <Stack direction="row" justifyContent="space-between">
          <LeftBar />
          <Feed />
          <Rightbar />
        </Stack>
        <Footer />
      </Box>
    </ThemeProvider>
  );
};

export default Home;
