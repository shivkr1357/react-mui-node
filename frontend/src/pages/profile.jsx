import { Box, createTheme, Stack, ThemeProvider } from "@mui/material";
import React from "react";
import { useState } from "react";
import LeftBar from "../components/LeftBar";
import Navbar from "../components/Navbar";
import ProfileComponent from "../components/ProfileComponent";

const Profile = () => {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });

  return (
    <ThemeProvider theme={darkTheme}>
      <Navbar setMode={setMode} mode={mode} />
      <Stack direction="row" justifyContent="space-between">
        <LeftBar />
        <ProfileComponent />
        <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}></Box>
      </Stack>
    </ThemeProvider>
  );
};

export default Profile;
