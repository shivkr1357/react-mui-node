import { ThemeProvider } from "@emotion/react";
import { createTheme } from "@mui/material";
import { Box, Stack } from "@mui/system";
import React, { useState } from "react";
import LeftBar from "../../components/LeftBar";
import Navbar from "../../components/Navbar";
import SinglePostComponent from "../../components/SinglePostComponent";

const SinglePost = () => {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });
  return (
    <ThemeProvider theme={darkTheme}>
      <Navbar setMode={setMode} mode={mode} />
      <Stack direction="row" justifyContent="space-between">
        <LeftBar />
        <SinglePostComponent />
      </Stack>
    </ThemeProvider>
  );
};

export default SinglePost;
