import { Box, createTheme, Stack, ThemeProvider } from "@mui/material";
import React from "react";
import { useState } from "react";
import LeftBar from "../../components/LeftBar";
import Navbar from "../../components/Navbar";
import Write from "../../components/Write";

const CreatePost = () => {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });
  return (
    <ThemeProvider theme={darkTheme}>
      <Navbar setMode={setMode} mode={mode} />
      <Stack direction="row" justifyContent="space-between">
        <LeftBar />
        <Write />
        <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}></Box>
      </Stack>
    </ThemeProvider>
  );
};

export default CreatePost;
