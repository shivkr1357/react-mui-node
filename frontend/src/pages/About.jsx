import { Box, createTheme, Stack, ThemeProvider } from "@mui/material";
import React, { useState } from "react";
import AboutComponent from "../components/AboutComponent";
import Footer from "../components/Footer";

import Navbar from "../components/Navbar";

const About = () => {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });
  return (
    <ThemeProvider theme={darkTheme}>
      <Box bgcolor={"background.default"} color={"text.primary"}>
        <Navbar setMode={setMode} mode={mode} />
        <Stack direction="row" justifyContent="space-between">
          <Box
            flex={1}
            p={2}
            sx={{ display: { xs: "none", sm: "block" } }}></Box>
          <AboutComponent />
          <Box
            flex={1}
            p={2}
            sx={{ display: { xs: "none", sm: "block" } }}></Box>
        </Stack>
        <Footer />
      </Box>
    </ThemeProvider>
  );
};

export default About;
