import { Box, createTheme, Stack, ThemeProvider } from "@mui/material";
import React, { useState } from "react";

import Navbar from "../components/Navbar";
import RegisterComponent from "../components/RegisterComponent";

const Register = () => {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });
  return (
    <ThemeProvider theme={darkTheme}>
      <Navbar setMode={setMode} mode={mode} />
      <Stack direction="row" justifyContent="space-between">
        <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}></Box>
        <RegisterComponent />
        <Box flex={1} p={2} sx={{ display: { xs: "none", sm: "block" } }}></Box>
      </Stack>
    </ThemeProvider>
  );
};

export default Register;
