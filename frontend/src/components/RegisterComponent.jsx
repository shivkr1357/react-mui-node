import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { register } from "../apis/auth";
import { PersonAddAlt1 } from "@mui/icons-material";

const RegisterComponent = () => {
  // const CLIENT_ID =
  //   "123524744887-jkijhcorv29hinbpiglrsdmpv4eiqh37.apps.googleusercontent.com";
  // const [imageUri, setImageUri] = useState("");
  // const [showRegisterButton, setShowRegisterButton] = useState(true);
  // const [showLogoutButton, setShowLogoutButton] = useState(false);
  const [values, setValues] = useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });
  const [inputField, setInputField] = useState({
    username: "",
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  const handleInput = (e) => {
    setInputField({ ...inputField, [e.target.name]: [e.target.value] });
    setValues({ ...values, password: e.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };
  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    let formData = new FormData();

    formData.append("email", inputField.email);
    formData.append("username", inputField.username);
    formData.append("password", inputField.password);

    register(formData)
      .then((res) => {
        navigate("/auth/login");
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  /*  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId:
          "123524744887-jkijhcorv29hinbpiglrsdmpv4eiqh37.apps.googleusercontent.com",
        scope: "email",
      });
    }

    gapi.load("client:auth2", start);
  }, []); */

  return (
    <Box flex={4} sx={{ padding: { xs: "20px", sm: "50px 20px " } }}>
      <form action="#">
        <Stack
          spacing={2}
          direction="column"
          sx={{
            marginLeft: { xs: "0", sm: "25%" },
            width: { xs: "100%", sm: "50%" },
          }}>
          <Stack
            spacing={2}
            direction="row"
            sx={{
              justifyContent: "center",
              alignItems: "center",
              color: "#3f51b5",
            }}>
            <PersonAddAlt1 sx={{ fontSize: "50px", fontWeight: "200" }} />
            <Typography
              variant="h3"
              component="h4"
              sx={{
                fontWeight: "200",
                fontSize: "50px",
                color: "#3f51b5",
              }}>
              Register
            </Typography>
          </Stack>
          <FormControl>
            <TextField
              id="outlined-multiline-flexible-username"
              label="Username"
              name="username"
              value={inputField.username}
              onChange={handleInput}
            />
          </FormControl>
          <FormControl>
            <TextField
              id="outlined-multiline-flexible-email"
              label="Email"
              name="email"
              value={inputField.email}
              onChange={handleInput}
            />
          </FormControl>
          <FormControl variant="outlined">
            <InputLabel htmlFor="outlined-adornment-password">
              Password
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={values.showPassword ? "text" : "password"}
              value={inputField.password}
              name="password"
              onChange={handleInput}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end">
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
          </FormControl>
          <Stack spacing={2} direction="row" sx={{ alignItems: "center" }}>
            <Button
              variant="outlined"
              color="info"
              onClick={handleSubmit}
              sx={{ width: "contained" }}>
              Register
            </Button>
            <Button
              variant="outlined"
              component="a"
              sx={{ textDecoration: "none", backgroundColor: "info" }}
              href="/auth/login">
              Login
            </Button>
          </Stack>
        </Stack>
      </form>
    </Box>
  );
};

export default RegisterComponent;
