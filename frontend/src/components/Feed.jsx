import { Box, CircularProgress, Stack, Typography } from "@mui/material";
import Post from "./Post";
import About from "./About";
import { useEffect, useRef, useState } from "react";
import { getInfiniteScroll } from "../apis/posts";
import { useNavigate } from "react-router-dom";

const Feed = () => {
  //   const posts = useSelector((state) => state.allPosts.posts);

  const [posts, setPosts] = useState([]);
  const [skip, setSkip] = useState(0);
  const [isEnd, setIsEnd] = useState(false);

  const ref = useRef();

  useEffect(() => {
    fetchPosts();
    ref.current?.addEventListener("scroll", handleScroll);
    // return () => ref.current?.removeEventListener("scroll", handleScroll);
  }, [skip]);
  const fetchPosts = async () => {
    try {
      const { data, error } = await getInfiniteScroll(skip);

      if (error) {
        console.log(error);
        return;
      }

      if (data?.length === 0) {
        setIsEnd(true);
        return;
      }
      // setPosts(data);
      setPosts([...posts, ...data]);
      console.log(posts.length);
    } catch (error) {
      console.log(error.message);
    }
  };

  const handleScroll = (e) => {
    const { offsetHeight, scrollTop, scrollHeight } = e.target;

    if (offsetHeight + scrollTop >= scrollHeight) {
      console.log(posts?.length);
      setSkip(posts?.length);
      console.log(skip);
    }

    console.log("skip : ", skip);
  };

  return (
    <Box flex={4} sx={{ padding: { xs: "0", sm: "0px 20px " } }}>
      <Box
        ref={ref}
        // onScroll={handleScroll}
        sx={{
          width: { xs: "100%", sm: "105% " },
          marginBottom: "50px",
          height: "600px",
          overflow: "scroll",
          overflowX: "hidden",
        }}>
        {posts.length > 0 ? (
          posts.map((post) => <Post key={post._id} {...post} />)
        ) : (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignSelf: "center",
              marginTop: "200px",
            }}>
            <CircularProgress
              sx={{
                alignSelf: "center",
              }}
            />
          </Box>
        )}
      </Box>

      <Box
        sx={{
          display: { sm: "none", xs: "block" },
          justifyContent: "center",
          alignItems: "center",
          paddingBottom: "50px",
        }}>
        <About />
      </Box>
    </Box>
  );
};

export default Feed;
