import { ThumbDown, ThumbUp } from "@mui/icons-material";
import { Box, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { getOnePost } from "../apis/posts";
import { getUser } from "../apis/user";

const SinglePostComponent = () => {
  const id = useParams();
  const [data, setData] = useState();
  const [user, setUser] = useState();

  useEffect(() => {
    getOnePost(id)
      .then((res) => {
        setData(res.data);
        getUser(res.data.user)
          .then((res) => {
            setUser(res);
          })
          .catch((error) => {
            console.log("error : ", error);
          });
      })
      .catch((error) => {
        console.log("error : ", error);
      });
  }, [id]);

  return (
    <Box flex={4} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
      <Stack spacing={3} p={2} alignContent="left">
        <Box>
          <img src={data ? data.photo : ""} alt="" />
        </Box>
        <Box>{data ? data.title : ""}</Box>
        <Box sx={{ ":first-letter": { fontSize: "35px" } }}>
          {data ? data.desc : ""}
        </Box>
        <Box>
          <Stack
            direction="row"
            spacing={1}
            sx={{
              justifyContent: "center",
              alignItems: "center",
              paddingBottom: "50px",
            }}>
            <Typography variant="h6"> Was this page helpful? </Typography>
            <ThumbUp sx={{ cursor: " pointer" }} />
            <ThumbDown sx={{ cursor: " pointer" }} />
          </Stack>
        </Box>
        <Box> Subscribe to the mail </Box>
        <Box> Footer </Box>
      </Stack>
    </Box>
  );
};

export default SinglePostComponent;
