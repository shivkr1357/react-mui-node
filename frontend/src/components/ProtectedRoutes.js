import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { isAuthenticated } from "../Helpers/auth";

const ProtectedRoutes = () => {
  return isAuthenticated() ? <Outlet /> : <Navigate to="/auth/login" />;
};

export default ProtectedRoutes;
