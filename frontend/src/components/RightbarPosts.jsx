import {
  Avatar,
  Box,
  Card,
  CircularProgress,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListSubheader,
  Typography,
} from "@mui/material";
import { Fragment } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { capitalize } from "../Helpers/Capitalize";
import SmartText from "../Helpers/SmartText";
const RightbarPosts = () => {
  const posts = useSelector((state) => state.allPosts.posts);

  const navigate = useNavigate();

  let renderList = "";

  renderList =
    posts.length > 0 ? (
      posts.map((post, key) => (
        <>
          <ListItem
            alignItems="flex-start"
            onClick={() => navigate("/posts/singlePost/" + post._id)}
            sx={{ cursor: "pointer" }}
            key={post._id}>
            <ListItemAvatar>
              <Avatar alt={Array.from(post.title)[0]} src={post.photo} />
            </ListItemAvatar>
            <ListItemText
              primary={capitalize(post.title)}
              secondary={
                <Fragment>
                  <Typography
                    sx={{ display: "inline" }}
                    component="span"
                    variant="body2"
                    color="text.primary">
                    Shiv Prasad
                  </Typography>
                  <SmartText text={post.desc} />
                </Fragment>
              }
            />
          </ListItem>
          <Divider variant="inset" component="li" />
        </>
      ))
    ) : (
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignSelf: "center",
          marginTop: "200px",
        }}>
        <CircularProgress
          sx={{
            alignSelf: "center",
          }}
        />
      </Box>
    );

  return (
    <List
      sx={{
        width: "100%",
        height: "100%",
        maxHeight: "100vh",
        maxWidth: "360px",
        overflowY: "scroll",
        bgcolor: "background.paper",
        position: "fixed",
      }}>
      <ListSubheader
        sx={{ position: "inherit" }}
        component="div"
        id="nested-list-subheader">
        RECENT POSTS
      </ListSubheader>
      {renderList}
    </List>
  );
};

export default RightbarPosts;
