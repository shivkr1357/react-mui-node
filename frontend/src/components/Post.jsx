import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Checkbox,
  IconButton,
} from "@mui/material";

import ShareIcon from "@mui/icons-material/Share";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Favorite from "@mui/icons-material/Favorite";
import { FavoriteBorder } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import SmartText from "../Helpers/SmartText";
import { capitalize } from "../Helpers/Capitalize";

const Post = ({ _id, desc, title, photo, caption, updatedAt }) => {
  const navigate = useNavigate();

  return (
    <Card sx={{ marginBottom: "20px" }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: "red" }} aria-label="recipe">
            {Array.from(title)[0]}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={capitalize(title)}
        subheader={updatedAt}
        onClick={() => {
          navigate("/posts/singlePost/" + _id);
        }}
        sx={{ cursor: "pointer" }}
      />
      <CardMedia component="img" height="20%" image={photo} alt="Paella dish" />
      <CardContent>
        <SmartText text={desc} />
        {/* <Typography variant="body2" color="text.secondary">
              {post.desc}
            </Typography> */}
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <Checkbox
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite sx={{ color: "red" }} />}
          />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        {/*  <ExpandMore
            expand={expanded}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </ExpandMore> */}
      </CardActions>
    </Card>
  );
};

export default Post;
