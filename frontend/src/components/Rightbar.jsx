import React from "react";
import { Box, Divider } from "@mui/material";

import RightbarPosts from "./RightbarPosts";

const Rightbar = () => {
  return (
    <Box flex={2} sx={{ display: { xs: "none", sm: "block" } }}>
      <Box position="relative">
        <Box>
          <RightbarPosts />
        </Box>
        <Divider />
      </Box>
    </Box>
  );
};

export default Rightbar;
