import { Box } from "@mui/material";

const Footer = () => {
  return (
    <Box flex={6} p={2} sx={{ display: { xs: "none", sm: "block" } }}>
      Footer
    </Box>
  );
};

export default Footer;
