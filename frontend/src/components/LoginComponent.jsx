import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GoogleLogin, GoogleLogout } from "react-google-login";
import { gapi } from "gapi-script";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { Visibility, VisibilityOff, Login } from "@mui/icons-material";
import { login } from "../apis/auth";
import { setAuthentication } from "../Helpers/auth";

const LoginComponent = () => {
  const CLIENT_ID =
    "123524744887-jkijhcorv29hinbpiglrsdmpv4eiqh37.apps.googleusercontent.com";
  const [imageUri, setImageUri] = useState("");
  const [showLoginButton, setShowLoginButton] = useState(true);
  const [showLogoutButton, setShowLogoutButton] = useState(false);
  const [values, setValues] = useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });
  const [inputField, setInputField] = useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  const handleInputChange = (e) => {
    setValues({ ...values, password: e.target.value });
    setInputField({ ...inputField, [e.target.name]: [e.target.value] });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };
  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };

  const handleSubmit = (e) => {
    let formData = new FormData();

    formData.append("email", inputField.email);
    formData.append("password", inputField.password);

    login(formData)
      .then((res) => {
        console.log(res.data);
        setAuthentication(res.data.token, res.data);
        navigate("/");
      })
      .catch((error) => {
        console.log("error", error);
      });

    e.preventDefault();
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId:
          "123524744887-jkijhcorv29hinbpiglrsdmpv4eiqh37.apps.googleusercontent.com",
        scope: "email",
      });
    }

    gapi.load("client:auth2", start);
  }, []);
  const handleSuccess = (resSuccess) => {
    console.log("Success : ", resSuccess);
    setShowLogoutButton(true);
    setShowLoginButton(false);
    setImageUri(resSuccess.profileObj.imageUrl);
  };
  const handleFailure = (resError) => {
    console.log(" Error ", resError);
  };

  const onLogoutSuccess = () => {
    setShowLoginButton(true);
    setShowLogoutButton(false);
    setImageUri(null);
    alert("You have been logged out successfully");
  };

  return (
    <Box
      flex={4}
      sx={{
        padding: { xs: "20px", sm: "50px 20px " },
        backgroundImage: "/images/02.jpg",
      }}>
      <form>
        <Stack
          spacing={2}
          direction="column"
          sx={{
            marginLeft: { xs: "0", sm: "25%" },
            width: { xs: "100%", sm: "50%" },
          }}>
          <Stack
            spacing={2}
            direction="row"
            sx={{
              justifyContent: "center",
              alignItems: "center",
              color: "#3f51b5",
            }}>
            <Login sx={{ fontSize: "50px", fontWeight: "200" }} />
            <Typography
              variant="h3"
              component="h4"
              sx={{
                textAlign: "center",
                fontWeight: "200",
                fontSize: "50px",
                color: "#3f51b5",
              }}>
              Login
            </Typography>
          </Stack>

          <FormControl>
            <TextField
              id="outlined-multiline-flexible"
              label="Email"
              value={inputField.email}
              name="email"
              onChange={handleInputChange}
            />
          </FormControl>
          <FormControl variant="outlined">
            <InputLabel htmlFor="outlined-adornment-password">
              Password
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={values.showPassword ? "text" : "password"}
              value={inputField.password}
              name="password"
              onChange={handleInputChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end">
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
          </FormControl>
          <Stack spacing={2} direction="row">
            <Button
              variant="outlined"
              color="info"
              onClick={handleSubmit}
              sx={{ width: "contained" }}>
              Login
            </Button>
            <Button
              component="a"
              href="/auth/register"
              variant="outlined"
              color="info"
              sx={{ width: "contained" }}>
              Register
            </Button>
          </Stack>
        </Stack>
      </form>
      <Box
        sx={{
          width: "100%",
          marginTop: "20px",
          textAlign: "center",
        }}>
        {showLoginButton && (
          <GoogleLogin
            style={{ width: "500px" }}
            clientId={CLIENT_ID}
            buttonText="Login Using Google"
            onSuccess={handleSuccess}
            onFailure={handleFailure}
            cookiePolicy={"single_host_origin"}
          />
        )}
        {showLogoutButton && (
          <GoogleLogout
            clientId={CLIENT_ID}
            buttonText="Logout"
            onLogoutSuccess={onLogoutSuccess}></GoogleLogout>
        )}
      </Box>
      <Box>
        <img src={imageUri} alt="" />
      </Box>
    </Box>
  );
};

export default LoginComponent;
