import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
} from "@mui/material";
import { AddPhotoAlternate, FormatBold } from "@mui/icons-material";
import { useState } from "react";
import { useEffect } from "react";
import { fetchAll } from "../apis/categories";
import { createPosts } from "../apis/posts";

import { useNavigate } from "react-router-dom";

const Write = () => {
  let categories = [];
  var results;
  const navigate = useNavigate();
  const [categoriesArray, setCategoriesArray] = useState([]);
  const [image, setImage] = useState("");

  const [rendered, setRendered] = useState(false);
  const [inputField, setInputField] = useState({
    title: "",
    desc: "",
    photo: "",
    categories: "",
  });

  const handleCategoryChange = (e) => {
    setInputField({ ...inputField, categories: e.target.value });
  };
  const handleImageUpload = (e) => {
    setImage(e.target.files[0]);
    setInputField({ ...inputField, photo: e.target.files[0] });
  };
  const handleInputChange = (e) => {
    setInputField({ ...inputField, [e.target.name]: [e.target.value] });
  };

  const handleSubmit = () => {
    let formData = new FormData();

    formData.append("title", inputField.title);
    formData.append("desc", inputField.desc);
    formData.append("categories", inputField.categories);
    formData.append("photo", inputField.photo, inputField.photo.name);

    createPosts(formData)
      .then((res) => {
        navigate("/");
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  useEffect(() => {
    if (rendered) {
      fetchAll()
        .then((res) => {
          res.data.forEach((name) => {
            categories.push(name.name);
          });
          results = Object.values(categories);
          setCategoriesArray(results);
        })
        .catch((error) => {
          console.log("error : ", error);
        });
    } else {
      setRendered(true);
    }
  }, [rendered]);

  return (
    <Box flex={4} sx={{ padding: { xs: "0", sm: "50px 20px " } }}>
      <img
        style={{
          // marginLeft: "150px",
          width: "50vw",
          height: "250px",
          borderRadius: "10px",
          objectFit: "cover",
        }}
        src={image === "" ? "" : URL.createObjectURL(image)}
        alt="image"
        loading="lazy"
      />
      {/* <ImageListItemBar position="below" title="" /> */}

      <form style={{ position: "relative", marginTop: "50px" }}>
        <Stack spacing={2}>
          <Stack direction="row" spacing={1}>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="fileInput">
                <AddPhotoAlternate
                  fontSize="large"
                  sx={{
                    cursor: "pointer",
                  }}
                />
              </label>

              <input
                type="file"
                id="fileInput"
                style={{ display: "none" }}
                name="photo"
                onChange={handleImageUpload}
              />
            </Box>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="bold">
                <FormatBold
                  sx={{
                    cursor: "pointer",
                  }}
                  fontSize="large"
                />
              </label>
              <label>{image.name}</label>
            </Box>
            <Box sx={{ display: "flex", alignItems: "center" }}></Box>
          </Stack>
          <Box>
            <TextField
              sx={{ width: "100%" }}
              id="outlined-basic"
              label="Post Title"
              variant="outlined"
              name="title"
              onChange={handleInputChange}
            />
          </Box>
          <FormControl sx={{ minWidth: 120, width: "100%" }}>
            <InputLabel id="demo-simple-select-helper-label">
              Category
            </InputLabel>
            <Select
              labelId="demo-simple-select-helper-label"
              id="demo-simple-select-helper"
              value={inputField.categories}
              label="Category"
              onChange={handleCategoryChange}>
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {categoriesArray &&
                categoriesArray.map((value, key) => {
                  return (
                    <MenuItem key={key} value={value}>
                      {value}
                    </MenuItem>
                  );
                })}
            </Select>
          </FormControl>
          <Box>
            <TextField
              sx={{ width: "100%" }}
              id="outlined-multiline-flexible"
              label="Post Description"
              multiline
              rows={10}
              name="desc"
              onChange={handleInputChange}

              // value={value}
              // onChange={handleChange}
            />
          </Box>
        </Stack>
        <Button
          variant="contained"
          sx={{ marginTop: "20px", marginRight: "20px" }}
          onClick={handleSubmit}>
          Publish
        </Button>
        <Button
          variant="outlined"
          sx={{ marginTop: "20px", marginRight: "20px" }}>
          Cancel
        </Button>
        <Button variant="outlined" color="secondary" sx={{ marginTop: "20px" }}>
          Save Draft
        </Button>
      </form>
    </Box>
  );
};

export default Write;
