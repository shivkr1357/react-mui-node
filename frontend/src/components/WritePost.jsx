import React, { useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { Box, Button, Stack, Typography } from "@mui/material";

const WritePost = () => {
  const [text, setText] = useState("");
  return (
    <Stack spacing={2}>
      <Box>
        Editor
        <CKEditor
          editor={ClassicEditor}
          data={text}
          onChange={(e, editor) => {
            const data = editor.getData();
            setText(data);
          }}
        />
      </Box>
      <Box>
        <Button variant="contained">Post</Button>
      </Box>
      <Box>
        <Typography variant="h4">Content</Typography>
        <Typography> {text} </Typography>
      </Box>
    </Stack>
  );
};

export default WritePost;
