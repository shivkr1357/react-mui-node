import React, { useEffect } from "react";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { Visibility, VisibilityOff } from "@mui/icons-material";

import { updateUser } from "../apis/user";
import { getCookie } from "../Helpers/cookies";
import { getLocalStorage } from "../Helpers/localStorage";

const ProfileComponent = () => {
  const [image, setImage] = useState("");
  const [inputField, setInputField] = useState({
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    confirm_password: "",
    profile_pic: "",
  });
  const [values, setValues] = useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });

  const handleImageUpload = (e) => {
    setImage(e.target.files[0]);
    setInputField({ ...inputField, profile_pic: e.target.files[0] });
  };

  const handleInput = (e) => {
    setInputField({ ...inputField, [e.target.name]: [e.target.value] });
    setValues({ ...values, password: e.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };
  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };

  let userDetails;

  useEffect(() => {
    if (getCookie("token") && getLocalStorage("user")) {
      userDetails = getLocalStorage("user");
    }
    console.log("user + ", userDetails);
  });

  const handleSubmit = () => {
    const formData = new FormData();

    formData.append(
      "profile_pic",
      inputField.profile_pic,
      inputField.profile_pic.name
    );
    formData.append("first_name", inputField.first_name);
    formData.append("last_name", inputField.last_name);
    formData.append("email", inputField.email);
    formData.append("username", inputField.username);
    formData.append("password", inputField.password);
    formData.append("confirm_password", inputField.confirm_password);

    // let formObject = Object.fromEntries(formData.entries());

    updateUser(formData)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log("error");
      });
  };

  return (
    <Box flex={4} sx={{ padding: { xs: "0", sm: "50px 20px " } }}>
      <Stack spacing={2} sx={{ padding: { xs: "10px", sm: "0px" } }}>
        <Box>
          <Typography
            variant="h3"
            sx={{ textAlign: "center", fontWeight: "100", fontSize: "40px" }}>
            Profile
          </Typography>
        </Box>
        <Stack
          spacing={2}
          direction="row"
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}>
          <Box>Shiv Shankar Prasad</Box>
          <Stack spacing={2} direction="row">
            <Button variant="outlined" color="error">
              Delete
            </Button>
          </Stack>
        </Stack>

        <form action="#" encType="multipart/form-data">
          <Stack spacing={2}>
            <Box sx={{ width: "75px", height: "75px" }}>
              <img
                src={image === "" ? "" : URL.createObjectURL(image)}
                alt=""
                style={{ width: "75px", height: "75px", alignSelf: "right" }}
              />
            </Box>
            <TextField
              label="First Name"
              id="first_name"
              name="first_name"
              variant="outlined"
              onChange={handleInput}></TextField>
            <TextField
              label="Last Name"
              id="last_name"
              name="last_name"
              variant="outlined"
              onChange={handleInput}></TextField>
            <TextField
              label="Email"
              id="email"
              name="email"
              variant="outlined"
              onChange={handleInput}></TextField>
            <TextField
              label="Username"
              id="username"
              name="username"
              variant="outlined"
              onChange={handleInput}></TextField>

            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={values.showPassword ? "text" : "password"}
                value={inputField.password}
                onChange={handleInput}
                name="password"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end">
                      {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
              />
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-confirm-password">
                Confirm Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-confirm-password"
                type={values.showPassword ? "text" : "password"}
                value={inputField.confirm_password}
                onChange={handleInput}
                name="confirm_password"
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end">
                      {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Confirm Password"
              />
            </FormControl>

            <Stack
              direction="row"
              spacing={2}
              sx={{
                width: "100%",
                alignItems: "center",
              }}>
              <Button variant="outlined" component="label" color="secondary">
                Select Image
                <input
                  hidden
                  accept="image/*"
                  multiple
                  name="profile_pic"
                  type="file"
                  onChange={handleImageUpload}
                />
              </Button>
              <label>{image.name}</label>
            </Stack>
            <Button
              variant="outlined"
              color="info"
              sx={{ width: "10%" }}
              onClick={handleSubmit}>
              Update
            </Button>
          </Stack>
        </form>
      </Stack>
    </Box>
  );
};

export default ProfileComponent;
