import { ActionTypes } from "../constants/action-types";

const INITIAL_STATE = {
  posts: [],
};

const postReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case ActionTypes.SET_POSTS:
      return { ...state, posts: payload };

    default:
      return state;
  }
};

export default postReducer;
