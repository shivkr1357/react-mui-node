import { combineReducers } from "redux";
import postReducer from "./postReducers";

const reducers = combineReducers({
  allPosts: postReducer,
});

export default reducers;
