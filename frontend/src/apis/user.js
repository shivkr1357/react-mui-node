import axios from "axios";

export const updateUser = async (data) => {
  const response = await axios.patch("/api/users/createUser", data);

  return response;
};

export const createUser = async (data) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };

  const response = await axios.post("/api/users/createUser", data, config);

  return response;
};

export const getUser = async (id) => {
  const response = await axios.get(`/api/auth/getUser/${id}`);

  return response;
};
