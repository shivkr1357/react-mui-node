import axios from "axios";
import { getCookie } from "../Helpers/cookies";

export const fetchPosts = async () => {
  const response = await axios.get("/api/posts/getPosts");

  return response;
};

export const createPosts = async (data) => {
  const token = getCookie("token");

  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  };

  const response = await axios.post("/api/posts/create", data, config);

  return response;
};

export const getOnePost = async (id) => {
  const response = await axios.get(`/api/posts/findOne/${id.id}`, id);

  return response;
};

// export const getInfiniteScroll = async (skip) => {
//   const config = {
//     headers: {
//       Accept: "application/json",
//       "Content-Type": "application/json",
//     },
//   };

//   const response = await axios.get(
//     `/api/posts/infiniteScroll?skip=${skip}`,
//     config
//   );

//   return await response.data.json();
// };

export const getInfiniteScroll = async (skip) => {
  try {
    const res = await fetch(`/api/posts/infiniteScroll?skip=${skip}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    return await res.json();
  } catch (error) {
    throw new Error(error);
  }
};
