import axios from "axios";

export const fetchAll = async () => {
  const response = await axios.get("/api/categories/fetchAll");

  return response;
};

export const createCategory = async () => {
  const response = await axios.post("/api/categories/createCategories");

  return response;
};
