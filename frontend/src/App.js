import { BrowserRouter, Routes, Route } from "react-router-dom";
import CreatePost from "./pages/Posts/CreatePost";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Register from "./pages/Register";
import ProtectedRoutes from "./components/ProtectedRoutes";
import SinglePost from "./pages/Posts/SinglePost";
import About from "./pages/About";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route path="/posts/create" element={<ProtectedRoutes />}>
          <Route path="/posts/create" element={<CreatePost />} />
        </Route>
        <Route path="/accounts/profile" element={<ProtectedRoutes />}>
          <Route path="/accounts/profile" element={<Profile />} />
        </Route>
        <Route path="/auth/login" element={<Login />}></Route>
        <Route path="/auth/register" element={<Register />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route
          exact
          path="/posts/singlePost/:id"
          element={<SinglePost />}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
