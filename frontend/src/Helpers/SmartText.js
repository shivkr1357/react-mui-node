import { useState } from "react";

const SmartText = ({ text, length = 100 }) => {
  const [showLess, setShowLess] = useState(true);

  if (text.length < length) {
    return <>{text}</>;
  }

  return (
    <div>
      <p
        dangerouslySetInnerHTML={{
          __html: showLess ? `${text.slice(0, length)}...` : text,
        }}></p>
      <a
        style={{ color: "blue", cursor: "pointer" }}
        onClick={() => setShowLess(!showLess)}>
        &nbsp;View {showLess ? "More" : "Less"}
      </a>
    </div>
  );
};

export default SmartText;
