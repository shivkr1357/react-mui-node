const router = require("express").Router();
const userController = require("../Controllers/userController");
const { protect } = require("../Middlewares/authenticator");

// Create Post
router.post("/updateUser", protect, userController.updateUser);

// // Fetch Post
// router.get("/getPosts", postController.getPosts);

// // Update Post
// router.patch("/update/:id", postController.updatePost);
// // Delete Post

// router.delete("/delete/:id", postController.deletePost);

// // Find One
// router.get("/findOne/:id", postController.findOne);

module.exports = router;
