const router = require("express").Router();
const postController = require("../Controllers/postController");
const { protect } = require("../Middlewares/authenticator");

// Create Post
router.post("/create", protect, postController.create);

// Fetch Post
router.get("/getPosts", postController.getPosts);

//Fetch all user posts
router.get("/getUserPosts", protect, postController.getUserPosts);

// Update Post
router.patch("/update/:id", protect, postController.updatePost);
// Delete Post

router.delete("/delete/:id", protect, postController.deletePost);

// Find One
router.get("/findOne/:id", postController.findOne);

//
router.get("/infiniteScroll", postController.getInfinitePost);

//fake data creation

router.post("/fakeDataCreation", postController.fakeDataCreation);

module.exports = router;
