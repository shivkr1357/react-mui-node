const router = require("express").Router();
const categoryController = require("../Controllers/categoryController");

//Fetch all
router.get("/fetchAll", categoryController.fetchAll);

// Create category
router.post("/createCategories", categoryController.createCategories);

module.exports = router;
