const router = require("express").Router();

const authController = require("../Controllers/authController");
const { protect } = require("../Middlewares/authenticator");

// Create User / Register
router.post("/register", authController.register);

// Login User

router.post("/login", authController.login);
router.get("/getUser/:id", authController.getUser);

module.exports = router;
